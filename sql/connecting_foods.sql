SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

--
-- Base de données :  `connecting_foods`
-- Moteur : MYSQL
--

DROP VIEW IF EXISTS farmer_certificate_view;
DROP VIEW IF EXISTS farmer_product_certificate_view;
DROP TABLE IF EXISTS certificate;
DROP TABLE IF EXISTS certif_type;
DROP TABLE IF EXISTS farmer_product;
DROP TABLE IF EXISTS farmer;
DROP TABLE IF EXISTS product;

CREATE TABLE farmer (
  fid int PRIMARY KEY NOT NULL AUTO_INCREMENT,
  name varchar(32) NOT NULL,
  siret varchar(14) NOT NULL,
  adresse varchar(256) NOT NULL
);

CREATE TABLE product (
  pid int PRIMARY KEY NOT NULL AUTO_INCREMENT,
  name varchar(32) NOT NULL,
  code_inter varchar(64) NOT NULL
);

CREATE TABLE farmer_product (
  fid int(11) NOT NULL,
  pid int(11) NOT NULL,
  unite int(6) NOT NULL,
  PRIMARY KEY (fid,pid),
  CONSTRAINT FK_farmer_fp FOREIGN KEY (fid) REFERENCES farmer(fid),
  CONSTRAINT FK_product_fp FOREIGN KEY (pid) REFERENCES product(pid)  
);

CREATE TABLE certif_type (
  ctid int PRIMARY KEY NOT NULL AUTO_INCREMENT,
  libelle varchar(16) NOT NULL
);

CREATE TABLE certificate (
  cid int PRIMARY KEY NOT NULL AUTO_INCREMENT,
  name varchar(32) NOT NULL,
  type int(11) NOT NULL,
  farmer int(11) NOT NULL,
  CONSTRAINT FK_type_c FOREIGN KEY (type) REFERENCES certif_type(ctid),
  CONSTRAINT FK_farmer_c FOREIGN KEY (farmer) REFERENCES farmer(fid)
);

CREATE VIEW farmer_product_certificate_view AS 
SELECT farmer.fid, farmer.name as farmer_name, farmer.siret, product.pid, product.name as product_name, product.code_inter, certificate.name as certificate, certif_type.libelle as certificate_type
FROM farmer, product, certif_type, certificate, farmer_product
WHERE farmer.fid = farmer_product.fid and product.pid = farmer_product.pid and farmer_product.fid = certificate.farmer and certif_type.ctid = certificate.type;

CREATE VIEW farmer_certificate_view AS
SELECT farmer.fid, farmer.name as farmer_name, certificate.cid, certif_type.ctid, certif_type.libelle as certificate_type
FROM farmer, certificate, certif_type
WHERE farmer.fid = certificate.farmer and certificate.type=certif_type.ctid;


INSERT INTO farmer(name, siret, adresse) VALUES
  ('Jean-Michel Dupont','12345678912345','4 Rue de la rue, 59000 Lille'), #1
  ('John Smith','42424242424242','67 Street\'s street, SW1A 2HQ Londres'), #2
  ('Juan Agricultor','98765432198765','42 Rue de la bonne idée, 64220 Saint-Jean-Pied-de-Port'); #3

INSERT INTO product(name, code_inter) VALUES
  ('Blé','lecodeduble'), #1
  ('Avoine','vivelavoine'), #2
  ('Raisin','dubonvin'), #3
  ('Houblon','delabiere'), #4
  ('Foie gras','joyeuxnoel'); #5

INSERT INTO farmer_product(fid,pid,unite) VALUES
  (1,1,500),
  (1,2,300),
  (2,1,350),
  (2,3,600),
  (2,4,1000),
  (3,5,300);

INSERT INTO certif_type (libelle) VALUES
 ('biologique'), #1
 ('sans ogm'), #2
 ('origine'); #3

INSERT INTO certificate (name,type,farmer) VALUES
  ('Pouce vert',1,1),
  ('Le Bon Certificat',2,2),
  ('Le Gentil Producteur',3,3),
  ('Vive le Nord',3,1);

COMMIT;