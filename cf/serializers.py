from rest_framework import serializers
from cf.models import *


class FarmerSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Farmer
        fields = '__all__'


class ProductSerializer(serializers.HyperlinkedModelSerializer):
    # producteurs = FarmerSerializer(many=True)

    class Meta:
        model = Product
        fields = '__all__'


class CertificateSerializer(serializers.HyperlinkedModelSerializer):
    # farmer = FarmerSerializer(many=False)

    class Meta:
        model = Certificate
        fields = '__all__'
