from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse
from django.template import loader
from rest_framework import viewsets, generics
from .serializers import *
from .models import Farmer, Product, Certificate


class FarmerViewSet(viewsets.ModelViewSet):
    queryset = Farmer.objects.all()
    serializer_class = FarmerSerializer


class ProductViewSet(viewsets.ModelViewSet):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer


class CertificateViewSet(viewsets.ModelViewSet):
    queryset = Certificate.objects.all()
    serializer_class = CertificateSerializer


class ViewFarmerPerCertificateType(generics.ListAPIView):
    serializer_class = FarmerSerializer

    def get_queryset(self):
        queryset = Farmer.objects.all()
        certif = self.request.query_params.get('certificate', None)

        if certif is not None:
            queryset = queryset.filter(certificate__type=certif)
        return queryset
