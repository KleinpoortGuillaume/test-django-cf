from enum import IntEnum


class CertificateTypes(IntEnum):
    BIOLOGIQUE = 1
    SANS_OGM = 2
    ORIGINE = 3

    @classmethod
    def choices(cls):
        return [(key.value, key.name) for key in cls]
