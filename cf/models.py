from django.db import models

from cf.utils import CertificateTypes


class Farmer(models.Model):
    nom = models.CharField(max_length=32)
    siret = models.CharField(max_length=14)
    adresse = models.CharField(max_length=256)

    class Meta:
        ordering = ['nom']

    def __str__(self):
        return f'farmer [' \
               f'nom: {self.nom}; ' \
               f'siret: {self.siret}; ' \
               f'adresse: {self.adresse}' \
               f']'


class Product(models.Model):
    nom = models.CharField(max_length=32)
    unite = models.IntegerField()
    code_inter = models.CharField(max_length=64)
    producteurs = models.ManyToManyField(Farmer)

    class Meta:
        ordering = ['nom']

    def __str__(self):
        return f'product [' \
               f'nom: {self.nom}; ' \
               f'unite: {self.unite}; ' \
               f'code_inter: {self.code_inter}; ' \
               f'producteurs:' "(%s)" % (
                ", ".join(prod.__str__() for prod in self.producteurs.all()),
                ) + ']'


class Certificate(models.Model):
    nom = models.CharField(max_length=32)
    type = models.IntegerField(choices=CertificateTypes.choices())
    farmer = models.ForeignKey(Farmer, on_delete=models.CASCADE)

    class Meta:
        ordering = ['nom']

    def __str__(self):
        return f'certificate [' \
               f'nom: {self.nom}; ' \
               f'type: {self.type}; ' \
               f'farmer: {self.farmer}' \
               f']'
