from django.contrib import admin

from cf.models import Farmer, Product, Certificate

admin.site.register(Farmer)
admin.site.register(Product)
admin.site.register(Certificate)
